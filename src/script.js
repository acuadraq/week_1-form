import {
  validateNames,
  validatePasswords,
  validatePhone,
  validateAge,
  validateEmail,
  validateUrl,
} from "./validations.js";
const form = document.querySelector(".div__form");
const firstName = document.getElementById("firstName");
const lastName = document.getElementById("lastName");
const email = document.getElementById("email");
const pwd = document.getElementById("pwd");
const pwdConfirmation = document.getElementById("pwdConfirmation");
const phone = document.getElementById("phone");
const age = document.getElementById("age");
const website = document.getElementById("website");
const experience = document.getElementById("experience");
const notification = document.getElementById("notification");
const keysPressed = [];
const konamiCode = "iamiron24";
const modal = document.querySelector(".modal");
const modalInformation = document.querySelector(".modal__correct");
export const userInfo = {};

//This is the function where it listens to the keys pressed to activated the konami code
window.addEventListener("keyup", (e) => {
  keysPressed.push(e.key.toLowerCase());
  keysPressed.splice(
    -konamiCode.length - 1,
    keysPressed.length - konamiCode.length
  );
  //When the keys matches with the konamicode it displayes the modal
  if (keysPressed.join("").includes(konamiCode)) {
    modal.style.display = "block";
    setTimeout(() => {
      modal.style.display = "none";
    }, 3000);
  }
});

//This convert the experience from the range to a String
const valueExperience = xp =>
  xp <= 30
    ? (userInfo.experience = "Junior Level")
    : xp > 30 && xp <= 60
    ? (userInfo.experience = "Mid Level")
    : (userInfo.experience = "Senior Level");

//This is the event for the form when is submitted
form.addEventListener("submit", (e) => {
  e.preventDefault();
  for (let info in userInfo) delete userInfo[info];
  validateNames(firstName, "first");
  validateNames(lastName, "last");
  validatePasswords(pwd.value, pwdConfirmation.value);
  validatePhone(phone);
  validateAge(age);
  validateEmail(email);
  validateUrl(website);
  valueExperience(experience.value);
  userInfo.notifications = notification.checked;
  //This checks if the userinfo is correct because you need 9 keys
  if (Object.keys(userInfo).length == 9) {
    console.table(userInfo);
    modalInformation.style.display = "block";
    setTimeout(() => {
      modalInformation.style.display = "none";
      form.reset();
    }, 3000);
  }
});
