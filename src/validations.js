let warningPwd = document.querySelector(".warningPwd");
let warningPwdConfirm = document.querySelector(".warningPwdConfirm");
import { userInfo } from "./script.js";

//This is the function to show the spans and give the styles to the errors
const showWarnings = field => {
  field.classList.add('warningStyle');
  field.nextElementSibling.classList.remove('warningBasic');
};

//This is the function to hide the spans and remove the styles from the errors
const removeWarnings = field => {
  field.classList.remove('warningStyle');
  field.nextElementSibling.classList.add('warningBasic');
};

const validateNames = (field, type) => {
  const validateLetters = /[^a-zA-Z]/g;
  if (field.value.trim() == "" || validateLetters.test(field.value)) {
    showWarnings(field);
  } else {
    switch (type) {
      case "first":
        userInfo.firstName = field.value;
        break;
      case "last":
        userInfo.lastName = field.value;
        break;
    }
    removeWarnings(field);
    return;
  }
};

const validateEmail = email => {
  const validEmail = /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}/;
  if(email.value.trim() == "" || !validEmail.test(email.value)){
    showWarnings(email);
  }else{
    userInfo.email = email.value;
    removeWarnings(email);
  }
}

const validatePasswords = (pwd_1, pwd_2) => {
  const validatePasswords = /(?=.*?[A-Z])(?=.*?\d)(?=.*?\w).{8}$/
  warningPwd.textContent = "";
  warningPwdConfirm.textContent = "";
  if (!validatePasswords.test(pwd_1)) {
    warningPwd.textContent = "Password length must be atleast 8 characters, contain a special caracther, a capital letter and a number";
    warningPwd.previousElementSibling.classList.add("warningStyle");
    return;
  }else if (pwd_1 !== pwd_2) {
    warningPwd.textContent = "Passwords must match";
    warningPwd.previousElementSibling.classList.add("warningStyle");
    warningPwdConfirm.previousElementSibling.classList.add("warningStyle");
    warningPwdConfirm.textContent = "Passwords must match";
  } else {
    warningPwd.previousElementSibling.classList.remove("warningStyle");
    warningPwdConfirm.previousElementSibling.classList.remove("warningStyle");
    userInfo.password = pwd_1;
    return;
  }
};

const validatePhone = number => {
  const valid = /^([0-9]{4})[- ]?([0-9]{4})$/;
  if (valid.test(number.value)) {
    userInfo.phoneNumber = number.value;
    removeWarnings(number);
    return;
  } else {
    showWarnings(number);
  }
};

const validateAge = age => {
  const validateNumbers = /^\d{1,2}$/;
  if (validateNumbers.test(age.value)) {
    userInfo.age = age.value;
    removeWarnings(age);
    return;
  } else {
    showWarnings(age);
  }
};

const validateUrl = url => {
  const validUrl = /https?:\/\/(www\.)?[A-Za-z0-9]+\.[A-Za-z]{2,4}\/?.*/
  if(validUrl.test(url.value)){
    userInfo.website = url.value;
    removeWarnings(url);
  } else {
    showWarnings(url);
  }
};


export { validateNames, validatePhone, validatePasswords, validateAge, validateEmail, validateUrl};
