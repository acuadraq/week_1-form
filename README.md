# Week_1 Form
![](src/img/Screenshot.png)

## Description

This is the weekly assignment for the first week. Its a form that requests information of a different type to the user. This are the main points of the page:
- Validations of First and Last Name (No special characters or numbers)
- Validation of phone number. Only accepts 5555-5555, 5555 5555 or 55555555
- Validation of password. The password must be atleast 8 characters
- Validation of age
- Validation of email
- Validation of website url
- Every error will be displayed to the user in the input where the error occur
- Once the information is correct will be displayed on the console

Also there is a little easter egg(Konami Code Script). This easter egg is activated when you press "iamiron24" on your keyboard. Once you pressed all the keys right, a gif will appear for 3 seconds.